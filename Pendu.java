import java.util.Arrays ;
 
class Pendu{
	void principal(){
		
		partie(creerDico());
		
		}

	/**
	* lancement d’une partie du jeu du pendu
	* @param dico dictionnaire des mots à deviner
	*/	
	void partie(String[] dico){
		String mot = choisirMot(dico);
		
		char[] reponse = creerReponse(mot.length());
		afficherReponse(reponse);
		int vie = 9;
		
		while (vie > 0 && !estComplet(mot, reponse)){

			System.out.println("");
			char car ;
			car = SimpleInput.getChar("Testez une lettre  : ");
			System.out.println("");
			
			if (tester(mot, reponse, car)){

				System.out.println(" Allez-y continuez !");
				System.out.println("");

			}else{

				vie--;
				System.out.println("");
				System.out.println("Ah non ! Il vous reste " + vie + " vie");

			}
				
			afficherReponse(reponse);
		
		}
		
		if (estComplet(mot, reponse)){

			System.out.println("");
			System.out.println("Bien joué à vous, vous avez gagné !");
			System.out.println("");

		}else{

			System.out.println("");
			System.out.println("Dommage, le mot était : " + mot + ", une prochaine fois peut-être.");
			System.out.println("");

		}
	}
		
	/**
	* Création d'un dictionnaire de mots
	* @return dictionnaire initialisé
	*/
	String[] creerDico(){
		String [] dictionnaire ={"tristan","patrice","foidjou","doug","flatline","ballin","smile","feather","nujabes","drake","eminem"};
		return dictionnaire ;
	}

	/**
	 * Teste de creerDico
	 */
	void testcreerDico(String[] dictionnaire){
		System.out.println("Affiche mot du dictionnaire");
		System.out.println(Arrays.toString(dictionnaire));
	}

	/**
	* choix aléatoire d’un mot dans un dictionnaire
	* @param dico dictionnaire des mots à choisir
	* @return chaine choisie de manière aléatoire
	*/
	String choisirMot (String[] dictionnaire){
		int mot ;
		mot = (int) (Math.random() * 11 );
		
		//System.out.println("Le mot est : " + dictionnaire[mot]);

		System.out.println("");
		System.out.println("Il est l'heure de jouer au pendu , bon courage à vous !");

		return dictionnaire[mot];
	}

	/**
	* affiche la réponse du joueur
	* @param reponse reponse du joueur
	*/
	void afficherReponse(char[] reponse) {
		
		for (int i = 0 ; i < reponse.length ; i++) {
			System.out.print (reponse[i] + " ");
		}
		System.out.println ();
	}
	
	/**
	* création d’un tableau de reponse contenant des ’_’
	* @param lg longueur du tableau à créer
	* @return tableau de reponse contenant des ’_’
	*/
	char[] creerReponse(int lg){
		char[] rep = new char[lg];
		int i = 0;
		while (i < lg){
			rep [i] = '_';
			i++ ;
		}
		return rep ;
	}
	
	/**
	* teste la présence d’un caractère dans le mot
	* et le place au bon endroit dans réponse
	* @param mot mot à deviner
	* @param reponse réponse à compléter si le caractère est présent dans le mot
	* @param carccaractère à chercher dans le mot
	* @return vrai ssi le caractère est dans le mot à deviner
	*/
	boolean tester (String mot, char[] reponse, char car){
		
		int a = 0 ;
		boolean ret = false ;
		
		while(a != mot.length()){
			
			if (mot.charAt(a)== car){
				ret = true ;
				reponse [a] = car ;
				a++ ;
			}else{
				a++ ;
			}
		}

		return ret;
	}
	
	/**
	* rend vrai ssi le mot est trouvé
	* @param mot mot à deviner
	* @param reponse réponse du joueur
	* @return vrai ssi le mot est égal caractère par caractère à la réponse
	*/
	boolean estComplet (String mot, char[] reponse){
		boolean reus = true ;
		for(int i = 0 ; i < mot.length(); i++){
			
			if (mot.charAt(i) != reponse[i]){
				reus = false ;
			}
		}
		return reus ;
	}	
}
